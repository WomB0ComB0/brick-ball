from config import *
import pygame
from pygame.locals import *
import time
import random

class Paddle(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((PADDLE_WIDTH, PADDLE_HEIGHT))
        self.image.fill(BLUE)
        self.rect = self.image.get_rect()
        self.rect.x = (WINDOW_WIDTH - PADDLE_WIDTH) // 2
        self.rect.y = WINDOW_HEIGHT - PADDLE_HEIGHT - 10
        self.speed = 7

    def update(self, keys):
        if keys[pygame.K_LEFT] and self.rect.x > 0:
            self.rect.x -= self.speed
        if keys[pygame.K_RIGHT] and self.rect.x < WINDOW_WIDTH - PADDLE_WIDTH:
            self.rect.x += self.speed

class Ball(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface((BALL_RADIUS * 2, BALL_RADIUS * 2), pygame.SRCALPHA)
        pygame.draw.circle(self.image, RED, (BALL_RADIUS, BALL_RADIUS), BALL_RADIUS)
        self.rect = self.image.get_rect()
        self.rect.x = WINDOW_WIDTH // 2
        self.rect.y = WINDOW_HEIGHT // 2
        self.dx = BALL_X_VELOCITY
        self.dy = BALL_Y_VELOCITY

    def update(self):
        self.rect.x += self.dx
        self.rect.y += self.dy

        if self.rect.left <= 0 or self.rect.right >= WINDOW_WIDTH:
            self.dx *= -1
        if self.rect.top <= 0:
            self.dy *= -1
        if self.rect.bottom >= WINDOW_HEIGHT:
            return False
        return True

    def check_collision(self, paddle, bricks):
        if pygame.sprite.collide_rect(self, paddle):
            if self.rect.bottom <= paddle.rect.top + self.dy:
                self.dy *= -1
                self.rect.bottom = paddle.rect.top
            elif self.rect.top >= paddle.rect.bottom - self.dy:
                self.dy *= -1
                self.rect.top = paddle.rect.bottom
                self.dx *= -1
                self.rect.right = paddle.rect.left
            elif self.rect.left >= paddle.rect.right - self.dx:
                self.dx *= -1
                self.rect.left = paddle.rect.right

        collided_bricks = pygame.sprite.spritecollide(self, bricks, True)
        if collided_bricks:
            self.dy *= -1

class Brick(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((BRICK_WIDTH, BRICK_HEIGHT))
        self.color= random.choice(BRICK_COLORS)
        self.image.fill(self.color)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

class Game:
    def __init__(self) -> None:
        pygame.init()
        self.running = True
        pygame.display.set_caption("Breakout")
        self.surface = pygame.display.set_mode((WINDOW_WIDTH,WINDOW_HEIGHT))
        self.surface.fill(WHITE)

        self.paddle = Paddle()
        self.ball = Ball()
        self.all_sprites = pygame.sprite.Group()
        self.bricks = pygame.sprite.Group()

        self.all_sprites.add(self.paddle)
        self.all_sprites.add(self.ball)

        for i in range(ROWS_OF_BRICKS):
            for j in range(BRICKS_PER_ROW):
                brick = Brick(j * (BRICK_WIDTH + BRICK_GUTTER_WIDTH) + 10, i * (BRICK_HEIGHT + BRICK_GUTTER_WIDTH) + BRICK_Y_OFFSET)
                self.bricks.add(brick)
                self.all_sprites.add(brick)

    def run(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            keys = pygame.key.get_pressed()
            self.paddle.update(keys)
            if not self.ball.update():
                self.running = False

            self.ball.check_collision(self.paddle, self.bricks)

            self.surface.fill(BLACK)
            self.all_sprites.draw(self.surface)
            pygame.display.flip()
            time.sleep(SLEEP_TIME)
        pygame.quit()

if __name__ == "__main__":
    game = Game()
    game.run()